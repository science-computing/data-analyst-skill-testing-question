# Data Analysis of Student Graduation

The following is intended as a small test for the purposes of evaluating the abilities of potential candidates for the Data Analysis position in the Faculty of Science.

Presented within this repository are 2 datasets:
- *assignments.csv* which is a large file consisting of student grades for assignments since 2014
- *sqlite_data_final.sqlite* which is an SQLite database containing information about students, their programs, and the courses they have taken for cohorts since 2014, as well as the assignment weights for each assignment in each course.

The following inormation is provided about the data:

- Each program consists of 4 years of courses
- Each program admits a cohort of 200 students per year
- If a students fails a course, they do not retake it
- To pass a course, students must have an overall average of 50% or higher for said course
    - The weight of each assignment is available within the data
    - You will need to compute this overall average as part of the process
- Failing a course does not prevent a student from taking the next years' courses
- All students in each cohort take the same courses in the same order
- Students only take courses required for their program
- At the END of 4 years, any student with **more than 5 failed courses** cannot graduate
- Assignment grades are out of 100


For example, if we had a program that looked like this:
- YEAR 1
    - CHEM100
    - CHEM101
- YEAR 2
    - CHEM200
    - CHEM201
- YEAR 3
    - CHEM300
    - CHEM301
- YEAR 4
    - CHEM400
    - CHEM401


A student who failed CHEM100 and CHEM101 in year 1 would still continue to year 2 and take CHEM200 and CHEM201. The final number of failed courses isn't calculated until the end of year 4.


### The Task:
You goal is to predict which students in the Biology program may fail to graduate in **2025**. Also, to determine any relevant factors that may contribute to this. Note that while assignments have been generated for 2025, there are not yet any grades for these assignments. The prediction will need to be based on data from previous years.


You may use any tools or techniques at your disposal.


Be prepared to provide a 5-10 minute presentation of your findings, which should include:
- A visualisation of the data
- A list of student numbers for at-risk students
- A walk-through of methods you used to parse and analyse the data
- An explanation of factors impacting potential failures
